/**
 * created by Chinchien
 * -for redirecting to user's blog: view my blog & view other's blog
 * */
package Servlets;

import DAOs.*;
import org.owasp.encoder.Encode;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class HomePageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.addIntHeader("X-XSS-Protection", 1);
        resp.addHeader("X-Content-Type-Options", " nosniff");
        resp.addHeader("X-Frame-Options", "deny");

        User ownerInfo = null;
        List<Article> articles = null;
        List<Comment> comments = null;
        HttpSession sess = req.getSession(true);
        String username = Encode.forHtml((String) sess.getAttribute("username"))    ;
        //view my blog
        String status =  Encode.forHtml(req.getParameter("status"))  ;
        if (status != null && status.equals("viewMyBlog")) {
            if (username != null) {
                //get all user info
                try (UserDAO userDAO = new UserDAO()) {
                    ownerInfo = userDAO.getAllUserInfoByName(username);
                } catch (SQLException e) {
                    System.out.println("Exception occured");
                } catch (Exception e) {
                    System.out.println("Exception occured");
                }
                //get user's articles
                try (ArticleDAO articleDAO = new ArticleDAO()) {
                    articles = articleDAO.getArticlesByName(username);
                } catch (SQLException e) {
                    System.out.println("Exception occured");
                } catch (Exception e) {
                    System.out.println("Exception occured");
                }

                try (CommentDAO commentDAO = new CommentDAO()) {
                    comments = commentDAO.getAllComments();
                } catch (SQLException e) {
                    System.out.println("Exception occured");
                } catch (Exception e) {
                    System.out.println("Exception occured");
                }
            }
        }
        //view other's blog
        if (status != null && !status.equals("viewMyBlog")) {
            //get info
            try (UserDAO userDAO = new UserDAO()) {
                ownerInfo = userDAO.getAllUserInfoByName(status);
            } catch (SQLException e) {
                System.out.println("Exception occured");
            } catch (Exception e) {
                System.out.println("Exception occured");
            }
            //get articles
            try (ArticleDAO articleDAO = new ArticleDAO()) {
                articles = articleDAO.getArticlesByName(status);
            } catch (SQLException e) {
                System.out.println("Exception occured");
            } catch (Exception e) {
                System.out.println("Exception occured");
            }

            try (CommentDAO commentDAO = new CommentDAO()) {
                comments = commentDAO.getAllComments();
            } catch (SQLException e) {
                System.out.println("Exception occured");
            } catch (Exception e) {
                System.out.println("Exception occured");
            }
        }
        req.setAttribute("comments", comments);
        req.setAttribute("ownerInfo", ownerInfo);
        req.setAttribute("articles", articles);

        req.getRequestDispatcher("jsp/homePage.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
