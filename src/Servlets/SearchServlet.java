package Servlets;

import DAOs.Article;
import DAOs.ArticleDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@WebServlet(name = "SearchServlet")
public class SearchServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.addIntHeader("X-XSS-Protection", 1);
        response.addHeader("X-Content-Type-Options", " nosniff");
        response.addHeader("X-Frame-Options", "deny");


        //accept the post from the front end
        request.setCharacterEncoding("utf-8");

        String command = "%" + request.getParameter("command")+"%";
        String date = request.getParameter("date")+"%";
        String sortForward = request.getParameter("sortForward");
        String sortBackward = request.getParameter("sortBackward");

        System.out.println("dte"+ date);

        //sent the data to the front end
        request.setAttribute("command",command);
        request.setAttribute("date",date);
        request.setAttribute("sortForward",sortForward);
        request.setAttribute("sortBackward",sortBackward);
        List<Article> articles = new ArrayList<>();

        if (request.getParameter("command")!= null) {
            try(ArticleDAO articleDAO=new ArticleDAO()) {

                articles = articleDAO.getArticlesByName(command);
                articles.addAll(articleDAO.getArticlesByTitle(command));

                request.setAttribute("articles",articles);

            } catch (SQLException e) {
                System.out.println("no Article Name");
                e.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (request.getParameter("date")!= null) {
            try (ArticleDAO articleDAO=new ArticleDAO()){

                articles = articleDAO.getArticlesByDate(date);

                request.setAttribute("articles",articles);

            } catch (SQLException e) {
                System.out.println("no Article Date");
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (request.getParameter("sortForward")!= null) {
            try (ArticleDAO articleDAO=new ArticleDAO()){

                articles = articleDAO.getAllArticlesBySortingForward();
                request.setAttribute("articles",articles);
                request.setAttribute("articles", articles);

            } catch (SQLException e) {
                System.out.println("no Article Date");
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (request.getParameter("sortBackward")!= null) {
            try (ArticleDAO articleDAO=new ArticleDAO()){

                articles = articleDAO.getAllArticlesBySortingBackward();
                request.setAttribute("articles",articles);
                request.setAttribute("articles", articles);

            } catch (SQLException e) {
                System.out.println("no Article Date");
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        request.getRequestDispatcher("/jsp/mainPage.jsp").forward(request,response);


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
