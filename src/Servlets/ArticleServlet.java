package Servlets;

import DAOs.Article;
import DAOs.ArticleDAO;
import DAOs.Comment;
import DAOs.CommentDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;


import SecurityPack.AuthenticationModule;
import org.owasp.encoder.Encode;
import org.owasp.html.PolicyFactory;
import org.owasp.html.Sanitizers;


public class ArticleServlet extends HttpServlet {

    org.owasp.html.PolicyFactory supplement = new org.owasp.html.HtmlPolicyBuilder().allowUrlProtocols("data").toFactory();

    PolicyFactory policy = Sanitizers.BLOCKS.and(Sanitizers.IMAGES).and(Sanitizers.STYLES).and(Sanitizers.BLOCKS).and(Sanitizers.LINKS).and(supplement);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //for displaying
        if (req.getSession().getAttribute("image")==null){
            String image="avatar_default.png";

            req.getSession().setAttribute("image",image);
        }

        resp.addIntHeader("X-XSS-Protection", 1);
        resp.addHeader("X-Content-Type-Options", " nosniff");
        resp.addHeader("X-Frame-Options", "deny");
        displayArticlesList(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.addIntHeader("X-XSS-Protection", 1);
        resp.addHeader("X-Content-Type-Options", " nosniff");
        resp.addHeader("X-Frame-Options", "deny");

        Cookie[] cookies = req.getCookies();
//Here we loop through all cookies and determine which functionality is required

        for (int i = 0; i < cookies.length; i++) {
            String value = cookies[i].getValue();
            if (value.equals("true")) {
                String trustedTextArea= policy.sanitize(Encode.forHtml(req.getParameter("editTextArea")) );
                String trustedTitle = Encode.forHtml(req.getParameter("editTitle"));
                String trustedID = Encode.forHtml(req.getParameter("editID"));

                req.getSession().setAttribute("textToGive", trustedTextArea);
                req.getSession().setAttribute("titleToGive", trustedTitle);
                req.getSession().setAttribute("iDToGive", trustedID);
                //deletes the cookie so that further business will not be confused.
                cookies[i].setMaxAge(0);

                modifyArticle(req, resp);
            }

            if (value.equals("newArticle")) {
                String trustedTitle = Encode.forHtml(req.getParameter("newTitle"));
                String trustedContent = policy.sanitize(Encode.forHtml(req.getParameter("newContent")));

                req.getSession().setAttribute("newContent", trustedContent);
                req.getSession().setAttribute("newTitle", trustedTitle);

                req.getSession().setAttribute("yearMD", req.getParameter("yearMD"));
                req.getSession().setAttribute("hourMS", req.getParameter("hourMS"));
               //deletes the cookie so that further business will not be confused.
                cookies[i].setMaxAge(0);
                makeNewArticle(req, resp);
            }

            if (value.equals("delete")) {
                String trustedDeleteTitle = Encode.forJava(req.getParameter("articleID"));
                cookies[i].setMaxAge(0);
                deleteArticle(req, resp, trustedDeleteTitle);
            }
        }
        try (ArticleDAO updatedArticles = new ArticleDAO()) {
            //gets the changes from the database and returns them to the main page
            List<Article> toSupply = updatedArticles.getAllArticles();
            req.getSession().setAttribute("articles", toSupply);
        } catch (Exception e) {
            System.out.println("Exception occured here");
        }

        req.getRequestDispatcher("/jsp/mainPage.jsp").forward(req, resp);

    }


    private void modifyArticle(HttpServletRequest req, HttpServletResponse resp) {
        try (ArticleDAO toFind = new ArticleDAO()) {
            HttpSession session = req.getSession();
            String id = (String) session.getAttribute("iDToGive");
            Article toModify = toFind.getArticleByID(id);

            AuthenticationModule toAuthenticate = new AuthenticationModule();
            //checks if the user has permission to conduct modification
            boolean safeToModify = toAuthenticate.check(session, toModify);
            if (safeToModify) {
                toFind.modifyArticle(session);
            } else {
                resp.sendRedirect("jsp/ERROR/noPermission.jsp");
//
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }


    private void makeNewArticle(HttpServletRequest req, HttpServletResponse resp) {
        boolean isSafeToCreate = false;

        HttpSession session = req.getSession();
        try (ArticleDAO makeNewArticle = new ArticleDAO()) {
            Article article = new Article();

            String newContent = (String) session.getAttribute("newContent");
            String newTitle = (String) session.getAttribute("newTitle");
            String userName = (String) session.getAttribute("username");
            String yearMD = (String) session.getAttribute("yearMD");
            System.out.println("yearMD" + yearMD);
            String hourMS = (String) session.getAttribute("hourMS");
            System.out.println("hourMS" + hourMS);

            //Chinchien: check the date format, make sure we get the correct format
            Date date = new Date();
            DateFormat formatForYear = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat formatForHours = new SimpleDateFormat("hh:mm:ss");
            if (yearMD.equals("")) {
                //current year
                yearMD = formatForYear.format(date);
            }

            if (hourMS.equals("")) {
                //current time
                hourMS = formatForHours.format(date);
            } else {
                hourMS = hourMS + ":00";
            }

            article.setTitle(newTitle);
            article.setContent(newContent);
            article.setUsername(userName);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            String modifiedDateAndTime = yearMD + " " + hourMS;
            article.setModifiedDateAndTime(dateFormat.parse(modifiedDateAndTime));

            AuthenticationModule toAuthenticate = new AuthenticationModule();
            isSafeToCreate = toAuthenticate.check(req.getSession(), article);

            if (isSafeToCreate) {
                makeNewArticle.createNewArticle(article);
            } else {
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/ERROR/noPermission.jsp");
                dispatcher.forward(req, resp);
            }
        } catch (Exception e) {
            System.out.println("Exception occured");
        }
    }

    private void displayArticlesList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //PS: (Chinchien): we should use non-nested try-resources instead of the nested one, because we can only create max 5 connections at the same time.

        //get all articles
        try (ArticleDAO articleDAO = new ArticleDAO()) {
            List<Article> articles = articleDAO.getAllArticles();
            // Adding the article list to the request object
            request.setAttribute("articles", articles);
        } catch (SQLException e) {
            System.out.println("SQLException occured ");
        } catch (Exception e) {
            System.out.println("Exception occured ");
        }
        //get all comments
        try (CommentDAO commentDAO = new CommentDAO()) {
            List<Comment> comments = commentDAO.getAllComments();
            List<Comment> nestedComments = commentDAO.getNestedComments();
            request.setAttribute("nestedComments", nestedComments);
            request.setAttribute("comments", comments);
        } catch (SQLException e) {
            System.out.println("SQLException occured ");
        } catch (Exception e) {
            System.out.println("Exception occured ");
        }
        request.getRequestDispatcher("/jsp/mainPage.jsp").forward(request, response);
    }


    private void deleteArticle(HttpServletRequest req, HttpServletResponse resp, String id) throws ServletException, IOException {
        //1.check if the action is safe
        boolean isSafeToDelete = false;
        try (ArticleDAO articleDAO = new ArticleDAO()) {
            Article articleToCheck = articleDAO.getArticleByID(id);
            AuthenticationModule toAuthenticate = new AuthenticationModule();
            isSafeToDelete = toAuthenticate.check(req.getSession(), articleToCheck);
        } catch (Exception e) {
            System.out.println("Exception occured ");
        }

        if (isSafeToDelete) {
            //2.delete the comments first, then article
            try (CommentDAO commentDAO = new CommentDAO()) {
                commentDAO.deleteAllComments(id);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //then delete the article.
            try (ArticleDAO articleDAO = new ArticleDAO()) {
                articleDAO.deleteArticle(id);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ERROR/noPermission.jsp");
            dispatcher.forward(req, resp);
        }
    }

}
